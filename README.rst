===============================
RxCheck XIO DVH Backend
===============================

.. image:: https://badge.fury.io/py/rxcheck-xio-dvh.png
    :target: http://badge.fury.io/py/rxcheck-xio-dvh

.. image:: https://travis-ci.org/tohccmedphys/rxcheck-xio-dvh.png?branch=master
        :target: https://travis-ci.org/tohccmedphys/rxcheck-xio-dvh

.. image:: https://pypip.in/d/rxcheck-xio-dvh/badge.png
        :target: https://pypi.python.org/pypi/rxcheck-xio-dvh


An RxCheck backend for importing DVH data exported from XIO

* Free software: BSD license
* Documentation: http://rxcheck-xio-dvh.rtfd.org.

Features
--------

* TODO

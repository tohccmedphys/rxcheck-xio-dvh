#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_xiodvh
----------------------------------

Tests for `xiodvh` module.
"""

import unittest
import datetime

import xiodvh.xiodvh as xiodvh
import xiodvh.settings as settings

class TestXIODVHSSH(unittest.TestCase):

    def setUp(self):
        from sensitive import XIO_SSH_SETTINGS, XIO_PATIENT_PATH

        self.old_dvh_find = xiodvh.settings.XIO_FIND_DVH_COMMAND
        self.old_dvh_path = xiodvh.settings.XIO_PATIENT_PATH
        self.old_settings = xiodvh.XIO_SETTINGS

        xiodvh.settings.XIO_FIND_DVH_COMMAND = settings._FIND_DVH_COMMAND.format(XIO_PATIENT_PATH, settings.XIO_DVH_FNAME)
        xiodvh.settings.XIO_PATIENT_PATH = XIO_PATIENT_PATH
        xiodvh.XIO_SETTINGS = XIO_SSH_SETTINGS

        self.maxDiff = None
        self.all_patients = {
            "01234567":{
                "course1" : {
                    "name":"course1",
                    "plan_date": datetime.datetime(year=2014, month=1, day=1, hour=23, minute=59, second=59),
                }
            }
        }

        self.patient_id = "01234567"
        self.patient_plan_id = "course1"
        self.structures = {
            "PTV" : {"volume":None},
            "CTV" : {"volume":None},
            "lt kidney": {"volume":None},
            "cord": {"volume":None},
        }


        self.importer = xiodvh.XIODVHImporter()

    def tearDown(self):
        xiodvh.settings.XIO_FIND_DVH_COMMAND =  self.old_dvh_find
        xiodvh.settings.XIO_PATIENT_PATH = self.old_dvh_path
        xiodvh.XIO_SETTINGS = self.old_settings

    def test_patients(self):
        patients =  self.importer.get_patients()
        self.assertTrue(patients)

    def test_plan_retrieve(self):
        patients =  self.importer.get_patients()
        pid, plans = patients.popitem()
        plan_id, plan_info = plans.popitem()

        plan = self.importer.get_patient_plan(pid, plan_id)
        self.assertIsNotNone(plan)

    def test_invalid_plan(self):
        plan = self.importer.get_patient_plan("00000000", "NOT A VALID PLAN")
        self.assertIsNone(plan)


class TestXIODVH(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.all_patients = {
            "01234567":{
                "course1" : {
                    "name":"course1",
                    "plan_date": datetime.datetime(year=2014, month=1, day=1, hour=23, minute=59, second=59),
                }
            }
        }



        self.patient_id = "01234567"
        self.patient_plan_id = "course1"
        self.structures = {
            "PTV" : {"volume":None},
            "CTV" : {"volume":None},
            "lt kidney": {"volume":None},
            "cord": {"volume":None},
        }


        self.importer = xiodvh.XIODVHImporter()

    def test_invalid_plan(self):
        plan = self.importer.get_patient_plan("00000000", "NOT A VALID PLAN")
        self.assertIsNone(plan)

    def test_invalid_rtog(self):
        plan = self.importer.get_patient_plan("99999999", "NOT A VALID PLAN")
        self.assertIsNone(plan)

    def test_patients(self):
        patients =  self.importer.get_patients()
        self.assertEqual(patients, self.all_patients)

    def test_structs(self):
        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
        self.assertSetEqual(set(plan["structures"].keys()),set(self.structures.keys()))

    def test_ssh_patients(self):
        from sensitive import XIO_SSH_SETTINGS, XIO_PATIENT_PATH

        old_dvh_find = xiodvh.settings.XIO_FIND_DVH_COMMAND
        old_dvh_path = xiodvh.settings.XIO_PATIENT_PATH
        old_settings = xiodvh.XIO_SETTINGS

        xiodvh.settings.XIO_FIND_DVH_COMMAND = settings._FIND_DVH_COMMAND.format(XIO_PATIENT_PATH, settings.XIO_DVH_FNAME)
        xiodvh.settings.XIO_PATIENT_PATH = XIO_PATIENT_PATH
        xiodvh.XIO_SETTINGS = XIO_SSH_SETTINGS
        patients =  self.importer.get_patients()

        xiodvh.settings.XIO_FIND_DVH_COMMAND =  old_dvh_find
        xiodvh.settings.XIO_PATIENT_PATH = old_dvh_path
        xiodvh.XIO_SETTINGS = old_settings

    def test_ssh_patients(self):
        from sensitive import XIO_SSH_SETTINGS, XIO_PATIENT_PATH

        old_dvh_find = xiodvh.settings.XIO_FIND_DVH_COMMAND
        old_dvh_path = xiodvh.settings.XIO_PATIENT_PATH
        old_settings = xiodvh.XIO_SETTINGS

        xiodvh.settings.XIO_FIND_DVH_COMMAND = settings._FIND_DVH_COMMAND.format(XIO_PATIENT_PATH, settings.XIO_DVH_FNAME)
        xiodvh.settings.XIO_PATIENT_PATH = XIO_PATIENT_PATH
        xiodvh.XIO_SETTINGS = XIO_SSH_SETTINGS
        patients =  self.importer.get_patients()

        xiodvh.settings.XIO_FIND_DVH_COMMAND =  old_dvh_find
        xiodvh.settings.XIO_PATIENT_PATH = old_dvh_path
        xiodvh.XIO_SETTINGS = old_settings
#    def test_volumes(self):
#        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
#        for name, props in self.structures.iteritems():
#            if props["volume"] is not None:
#                self.assertAlmostEqual(plan["structures"][name]["volume"], props["volume"])
#
#    def test_Gy_input(self):
#        plan = self.importer.get_patient_plan(self.patient_id, "vmat3")
#        dvh_data = plan["structures"]["Axilla II"]["dvh"]
#        self.assertAlmostEqual(dvh_data["doses"][1], 49.25*100)
#
#    def test_cGy_input(self):
#        plan = self.importer.get_patient_plan("12345678", "Course1+Course2")
#        dvh_data = plan["structures"]["bladder"]["dvh"]
#        self.assertAlmostEqual(dvh_data["doses"][1], 395)
#
#    def test_bad_dose_unit(self):
#        with self.assertRaises(ValueError):
#            plan = self.importer.get_patient_plan("12345678", "bad_unit")
#
#    def test_bad_vol_unit(self):
#        with self.assertRaises(ValueError):
#            plan = self.importer.get_patient_plan("12345678", "bad_vol_unit")
#
#    def test_number_bins_correct(self):
#        plan = self.importer.get_patient_plan(self.patient_id, "vmat3")
#        dvh_data = plan["structures"]["Axilla II"]["dvh"]
#        self.assertEqual(len(dvh_data["doses"]), len(dvh_data["volumes"]))
#
#    def test_missing_file(self):
#        with self.assertRaises(ValueError):
#            self.importer.find_dvh_path("foo", "bar")
#
#
#    def test_load_differential(self):
#        with self.assertRaises(ValueError):
#            self.importer.get_patient_plan("BVMAT09ABS", "vmat")
#
if __name__ == '__main__':
    unittest.main()

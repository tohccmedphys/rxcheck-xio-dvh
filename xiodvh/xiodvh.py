from rxcheck_base_importer import BaseImporter

from . import settings
from fabric.api import run, get
from fabric.api import settings as fab_settings
from fabric.context_managers import hide


from io import StringIO
import collections
import datetime
import glob
import numpy
import os
import re


XIO_SETTINGS = {
    "host_string": settings.XIO_HOST,
    "user": settings.XIO_USER,
    "password": settings.XIO_PASSWORD,
    "warn_only": False,
}

REMOTE_HEAD_SPLIT_RE = '==> .*? <==[\r\n]+'

NO_NAME = "<No Name>"


class XIODVHImporter(BaseImporter):

    NAME = "XIO DVH"
    HANDLE = "xiodvh"

    def get_patients(self):
        """Find XIO plan info for all patients from either filesystem or remote computer."""

        _plan_info_retrieve = self._patients_ssh if XIO_SETTINGS["host_string"] else self._patients_fs
        plan_infos = _plan_info_retrieve()

        patients = {}
        for plan_info in plan_infos:
            patient_id = plan_info['patient_id']
            # filename = os.path.basename(path).split('.txt')[0]
            info = dict((k, plan_info[k]) for k in ["name", "plan_date"])

            if plan_info['patient_id'] in patients:
                patients[patient_id][plan_info['id']] = info
            else:
                patients[patient_id] = {plan_info['id']: info}
            #
            # if patient_id in patients:
            #     found_plan_id = False
            #     for p in patients[patient_id]:
            #         if patients[patient_id][p]['name'] == plan_info['id']:
            #             found_plan_id = p
            #
            #     if found_plan_id:
            #         found_filename = patients[patient_id][found_plan_id]['filename']
            #         patients[patient_id][plan_info['id'] + '__(' + found_filename + ')'] = patients[patient_id].pop(
            #             found_plan_id)
            #         patients[patient_id][plan_info['id'] + '__(' + filename + ')'] = info
            #     else:
            #         patients[patient_id][plan_info['id']] = info
            # else:
            #     patients[patient_id] = {
            #         plan_info['id']: info,
            #     }

        return patients

    def get_patient_plan(self, patient_id, plan_id):
        """Find XIO plan for patient from filesystem or remote computer."""
        _plan_retrieve = self._patient_plan_ssh if XIO_SETTINGS["host_string"] else self._patient_plan_fs
        return _plan_retrieve(patient_id)

    def _header_to_plan_info(self, header):
        """Process first 7 lines of XIO RTOG dvh"""

        if header[0].strip() != settings.XIO_HEADER_DEF:
            return None

        plan_info = {
            "patient_id": header[1].strip(),
            "name": header[2].strip() or NO_NAME,
            "id": header[2].strip() or NO_NAME,
            "plan_date": datetime.datetime.strptime(header[6].strip(), settings.XIO_DVH_DATE_FORMAT),
        }

        return plan_info

    def _patients_ssh(self):
        """return all available XiO patient ID's by connecting to a remote computer via ssh"""

        stdout = StringIO()
        with fab_settings(hide('aborts', 'running'), **XIO_SETTINGS):
            out = run(settings.XIO_FIND_DVH_COMMAND, stdout=stdout)

        headers = [x for x in re.split(REMOTE_HEAD_SPLIT_RE, out)]
        for header in headers:
            plan_info = self._header_to_plan_info(re.split("[\r\n]{1,2}", header))
            if plan_info:
                pid = plan_info['patient_id']
                outs = re.findall('patient/\.' + pid + '(.*?)/rtog_dvh.dat', out, re.DOTALL | re.MULTILINE)
                if len(outs) > 0:
                    plan_info['patient_id'] = '.' + pid + outs[0]
                yield plan_info

    def _patients_fs(self):
        """list all patients that are available in a locally accessible directory"""

        paths = [os.path.normpath(x) for x in glob.glob(settings.XIO_DVH_GLOBBER)]

        header_range = range(7)
        for path in paths:
            with open(path, "r") as f:
                header = [f.readline() for _ in header_range]

            plan_info = self._header_to_plan_info(header)
            if plan_info:
                yield plan_info

    def _patient_plan_ssh(self, patient_id):
        """return a single patient plan from remote computer"""

        get_dvh = settings.XIO_GET_DVH_FORMAT.format(path=settings.XIO_PATIENT_PATH, patient_id=patient_id, fname=settings.XIO_DVH_FNAME)
        stdout = StringIO()
        try:
            with fab_settings(hide('aborts', 'running'), **XIO_SETTINGS):
                out = get(get_dvh, local_path=stdout)
            dvh_dat = stdout.getvalue()
        except:
            return None

        plan = self.dvh_to_plan(dvh_dat)
        plan['patient_id'] = patient_id
        return plan

    def _patient_plan_fs(self, patient_id):
        """return a single patient plan from local filesystem"""

        path = os.path.join(settings.XIO_PATIENT_PATH, patient_id, settings.XIO_DVH_FNAME)
        try:
            f = open(path, "r")
            dvh_dat = f.read()
            f.close()
        except:
            return None
        return self.dvh_to_plan(dvh_dat)

    def dvh_to_plan(self, data):
        if "XiO Dose Volume Histogram" not in data:
            return None

        structures = data.strip().split("XiO Dose Volume Histogram")
        plan = {"structures": []}

        for structure in structures:
            if not structure:
                continue
            structure = structure.strip().split('\n')

            plan["name"] = structure[1].strip() or NO_NAME
            plan["id"] = plan["name"]
            plan["patient_id"] = structure[0].strip()

            structure_name = structure[2].strip()

            dvh_dat = [x.split(",") for x in structure[7:]]

            doses, volumes = zip(*dvh_dat)

            doses = numpy.array(doses, dtype=numpy.float)
            volumes = numpy.array(list(reversed(numpy.cumsum(list(reversed(numpy.array(volumes, dtype=numpy.float)))))))

            plan["structures"].append({
                "name": structure_name,
                "volume": volumes.max(),
                "dvh": {
                    "doses": list(doses),
                    "volumes": list(volumes/volumes.max()),
                },
            })

        return plan


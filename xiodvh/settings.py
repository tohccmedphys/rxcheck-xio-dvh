import os
import sys

try:
    from django.conf import settings
    settings.DEBUG
except:
    settings = {}

base =  os.path.dirname(__file__)
tests = os.path.join(base, "..", "tests")
test_dvhs = os.path.join(tests, "dvhs")


XIO_DVH_FNAME = getattr(settings, "XIO_DVH_FNAME", "rtog_dvh.dat")
XIO_PATIENT_PATH = getattr(settings, "XIO_PATIENT_PATH", test_dvhs).rstrip("/")

# leave host/user/password blank for locally accesible directory
XIO_HOST = getattr(settings, "XIO_HOST", "")
XIO_USER = getattr(settings, "XIO_USER", "")
XIO_PASSWORD = getattr(settings, "XIO_PASSWORD", "")

_FIND_DVH_COMMAND = "find -L {0} -maxdepth 2 -ignore_readdir_race -name '{1}' -exec head -v -n7 '{{}}' \;"
XIO_FIND_DVH_COMMAND = getattr(settings, "XIO_FIND_DVH_COMMAND", _FIND_DVH_COMMAND.format(XIO_PATIENT_PATH, XIO_DVH_FNAME))

_DVH_GLOBBER = os.path.join(XIO_PATIENT_PATH, "*", XIO_DVH_FNAME)
XIO_DVH_GLOBBER = getattr(settings, "XIO_DVH_GLOBBER", _DVH_GLOBBER)

XIO_GET_DVH_FORMAT = getattr(settings, "XIO_GET_DVH_FORMAT", "{path}/{patient_id}/{fname}")


XIO_HEADER_DEF = getattr(settings, "XIO_HEADER_DEF", "XiO Dose Volume Histogram")

XIO_DVH_DATE_FORMAT =  getattr(settings, "XIO_DVH_DATE_FORMAT","%a %b %d %H:%M:%S %Y")

